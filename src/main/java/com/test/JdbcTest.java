package com.test;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class JdbcTest {

    @Test
    public void test(){
        System.out.println("junit测试");
    }

    @Test
    public void testJdbc() throws Exception{
        //1、注册驱动
        // DriverManager.registerDriver(new Driver()); // 不建议使用
        Class.forName("com.mysql.cj.jdbc.Driver");

        //2、获取连接Connection
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hs_test?useSSL=false&serverTimezone=Asia/Shanghai",
                "root", "root");

        //3、得到执行sql语句的对象Statement
        Statement statement = connection.createStatement();


        //4、执行sql语句，并返回结果集
        ResultSet resultSet = statement.executeQuery("select * from student");
        // statement.executeUpdate();

        //5、处理结果
        while(resultSet.next()){
            System.out.println(resultSet.getObject("id"));
            System.out.println(resultSet.getObject("name"));
            System.out.println(resultSet.getObject("chinese"));
            System.out.println(resultSet.getObject("english"));
            System.out.println(resultSet.getObject("math"));
            System.out.println("-----------------");
        }


        //6、关闭资源
        resultSet.close();
        statement.close();
        connection.close();



    }
}
