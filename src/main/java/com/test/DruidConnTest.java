package com.test;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.junit.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

public class DruidConnTest {

    @Test
    public void testDruid(){

        try {
            // 1.加载配置文件
            Properties properties = new Properties();
            InputStream is = DruidConnTest.class.getClassLoader().getResourceAsStream("druid.properties");
            properties.load(is);


            // 2.获取连接池对象
            DataSource dataSource = DruidDataSourceFactory.createDataSource(properties);

            // 3.获取连接
            Connection connection = dataSource.getConnection();

            // 4.创建一个执行SQL的preparedStatement对象
            String sql = "select * from  student where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,5);

            // 5.执行SQL，并获取结果
            ResultSet resultSet = preparedStatement.executeQuery();


            // 6.处理结果
            while (resultSet.next()){
                System.out.println(resultSet.getObject("id"));
                System.out.println(resultSet.getObject("name"));
                System.out.println(resultSet.getObject("chinese"));
                System.out.println(resultSet.getObject("english"));
                System.out.println(resultSet.getObject("math"));
                System.out.println("-----------------");
            }

            is.close();
            resultSet.close();
            preparedStatement.close();
            connection.close();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
