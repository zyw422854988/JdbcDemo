package com.test;

import java.util.Scanner;

public class ScannerMain {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);

        String next = scanner.next();
        System.out.println(next);
    }
}
