package com.test.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class JdbcUtils {

    /**
     *  静态都是属于类： JdbcUtils.class
     *  非静态都是对象的
     */
    private static String url ;
    private static String username ;
    private static String password ;

    private static String driver;

    static {

        try {
            //此对象是用于加载properties文件数据的
            ResourceBundle resourceBundle = ResourceBundle.getBundle("jdbc") ;
            driver = resourceBundle.getString("driver");

            // 1.注册驱动
            Class.forName(driver);
            // 2.获取连接
            // url = "jdbc:mysql://localhost:3306/hs_test?useSSL=false&serverTimezone=Asia/Shanghai";
            // username = "root";
            // password = "root";
            url = resourceBundle.getString("url");
            username = resourceBundle.getString("username");
            password = resourceBundle.getString("password");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    /**
     * 获取连接
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    /**
     * 关闭资源
     */
    public static void close(ResultSet resultSet, Statement statement,Connection connection){
        //关闭资源
        if(resultSet!=null){
            try {
                resultSet.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            resultSet = null;
        }
        if(statement!=null){
            try {
                statement.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            statement = null;
        }
        if(connection!=null){
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            connection = null;
        }

    }
}
