package com.test;

import com.test.utils.JdbcUtils;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OOMTest {
    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    @Test
    public void oomTest(){
        try {
            // 获取连接
            connection = JdbcUtils.getConnection();

            // 获取preparedStatement对象
            preparedStatement = connection.prepareStatement("select * from order_info ");


            // 执行SQL
            resultSet = preparedStatement.executeQuery();

            List<OrderInfo> orderInfoList = new ArrayList<OrderInfo>();
            // 遍历结果集
            while (resultSet.next()){
                OrderInfo orderInfo = new OrderInfo();
                orderInfo.setId(resultSet.getInt("id"));
                orderInfo.setOrderNo(resultSet.getString("order_no"));
                orderInfoList.add(orderInfo);
                // System.out.println(resultSet.getObject("id"));
                // System.out.println(resultSet.getObject("order_no"));
                System.out.println("-----------------");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(resultSet,preparedStatement,connection);
        }
    }
}
