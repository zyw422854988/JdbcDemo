package com.test;

import com.test.utils.JdbcUtils;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JdbcTransactionTest {

    private Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    @Test
    public void testTransaction(){

        try {
            // 获取连接
            connection = JdbcUtils.getConnection();
            /**
             *  事务的使用：connection调用
             *  setAutoCommit:  false 开启事务
             *                  true  不开启事务
             */
            connection.setAutoCommit(false); // start transaction

            // 定义一个SQL
            String sql = "insert into student(name,chinese,english,math) " +
                    "values (?,?,?,?)";
            // 获取执行SQL的PreparedStatement对象
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,"老赵");
            preparedStatement.setDouble(2,76.5);
            preparedStatement.setDouble(3,70.5);
            preparedStatement.setDouble(4,86.5);

            // 执行SQL  并获取结果
            int i = preparedStatement.executeUpdate();
            System.out.println("插入结果：" + i);
            /*resultSet = preparedStatement.executeQuery();

            // 处理结果：遍历打印
            while (resultSet.next()){
                System.out.println(resultSet.getInt("id"));
                System.out.println(resultSet.getString("name"));
                System.out.println(resultSet.getObject("chinese"));
                System.out.println(resultSet.getObject("english"));
                System.out.println(resultSet.getObject("math"));
                System.out.println("-----------------");
            }*/
            // int a = 3/0;

            // 代码正常执行，提交
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                if (connection != null)
                    // 回滚
                    connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }finally {
            // 关闭资源
            JdbcUtils.close(resultSet,preparedStatement,connection);
        }

    }
}
