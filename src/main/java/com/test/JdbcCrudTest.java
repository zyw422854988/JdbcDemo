package com.test;

import com.test.utils.JdbcUtils;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class JdbcCrudTest {

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    @Test
    public void testInsert(){

        try {
            // 获取连接（包含注册驱动过程）
            connection = JdbcUtils.getConnection();

            // 创建一个statement对象
            // statement = connection.createStatement();
            String sql = "";
            preparedStatement = connection.prepareStatement("insert into student(name,chinese,english,math) " +
                    "values (?,?,?,?)");
            preparedStatement.setString(1,"老赵");
            preparedStatement.setDouble(2,56.9);
            preparedStatement.setDouble(3,87.5);
            preparedStatement.setDouble(4,92.8);


            // 执行SQL，并得到结果
            /*int i = statement.executeUpdate("insert into student(name,chinese,english,math)" +
                    " values ('老李',67.8,83.5,95.3)");*/
            int i = preparedStatement.executeUpdate();

            System.out.println("返回结果:" + i);

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(resultSet,statement,connection);
        }
    }

    @Test
    public void testSelect(){
        try {
            // 获取连接
            connection = JdbcUtils.getConnection();

            // 获取preparedStatement对象
            preparedStatement = connection.prepareStatement("select * from student where id = ?");

            preparedStatement.setInt(1,5);

            // 执行SQL
            resultSet = preparedStatement.executeQuery();

            // 遍历结果集
            while (resultSet.next()){
                System.out.println(resultSet.getObject("id"));
                System.out.println(resultSet.getObject("name"));
                System.out.println(resultSet.getObject("chinese"));
                System.out.println(resultSet.getObject("english"));
                System.out.println(resultSet.getObject("math"));
                System.out.println("-----------------");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(resultSet,preparedStatement,connection);
        }
    }

    @Test
    public void testUpdate(){
        try {
            // 获取连接
            connection = JdbcUtils.getConnection();

            // 获取preparedStatement对象
            String sql = "update student set chinese =?, english = ? , math = ? where id IN (?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1,88.4);
            preparedStatement.setDouble(2,78.5);
            preparedStatement.setDouble(3,56.8);
            preparedStatement.setInt(4,11);
            preparedStatement.setInt(5,10);

            int i = preparedStatement.executeUpdate();

            System.out.println("更新结果:" + i);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            JdbcUtils.close(resultSet,preparedStatement,connection);
        }
    }

    @Test
    public void testDelete(){
        try {
            // 获取连接
            connection = JdbcUtils.getConnection();

            // 获取preparedStatement对象
            String sql = "delete from student where id in(?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,10);
            preparedStatement.setInt(2,11);

            // 执行SQL
            int i = preparedStatement.executeUpdate();

            System.out.println("删除结果:" + i);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            // 关闭资源
            JdbcUtils.close(resultSet,preparedStatement,connection);
        }
    }

    @Test
    public void testSqlInject(){
        try {
            // 获取连接
            connection = JdbcUtils.getConnection();

            // 获取statement对象
            statement = connection.createStatement();

            String params = "303 or 1=1";
            // #{}   和  ${}

            // 执行SQL
            String sql = "select * from student where id = "+params;
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()){
                System.out.println(resultSet.getObject("id"));
                System.out.println(resultSet.getObject("name"));
                System.out.println(resultSet.getObject("chinese"));
                System.out.println(resultSet.getObject("english"));
                System.out.println(resultSet.getObject("math"));
                System.out.println("-----------------");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            // 关闭资源
            JdbcUtils.close(resultSet,preparedStatement,connection);
        }
    }

}
