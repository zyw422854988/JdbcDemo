package com.test;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class C3p0ConnTest02 {

    @Test
    public void testC3p02() throws SQLException {
        DataSource dataSource = new ComboPooledDataSource();

        Connection connection = dataSource.getConnection();

        // 获取执行SQL的对象
        // 4.创建一个执行SQL的preparedStatement对象
        String sql = "select * from  student where id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1,5);

        // 5.执行SQL，并获取结果
        ResultSet resultSet = preparedStatement.executeQuery();


        // 6.处理结果
        while (resultSet.next()){
            System.out.println(resultSet.getObject("id"));
            System.out.println(resultSet.getObject("name"));
            System.out.println(resultSet.getObject("chinese"));
            System.out.println(resultSet.getObject("english"));
            System.out.println(resultSet.getObject("math"));
            System.out.println("-----------------");
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();
    }
}
