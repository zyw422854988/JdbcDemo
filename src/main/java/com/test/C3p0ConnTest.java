package com.test;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.junit.Test;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class C3p0ConnTest {

    @Test
    public void testC3p0() throws SQLException, PropertyVetoException {
        // 获取一个连接池对象 -- 数据源
        // DataSource ds = new ComboPooledDataSource();
        ComboPooledDataSource ds  = new ComboPooledDataSource();
        ResourceBundle rb = ResourceBundle.getBundle("c3p0");
        ds.setDriverClass(rb.getString("driverClass"));
        ds.setJdbcUrl(rb.getString("url"));
        ds.setUser(rb.getString("username"));
        ds.setPassword(rb.getString("password"));
        ds.setInitialPoolSize(Integer.parseInt(rb.getString("initialSize")));
        ds.setMaxPoolSize(Integer.parseInt(rb.getString("maxActive")));
        ds.setCheckoutTimeout(Integer.parseInt(rb.getString("maxWait")));

        // 获取连接
        Connection connection = ds.getConnection();

        // 获取执行SQL的对象
        // 4.创建一个执行SQL的preparedStatement对象
        String sql = "select * from  student where id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1,5);

        // 5.执行SQL，并获取结果
        ResultSet resultSet = preparedStatement.executeQuery();


        // 6.处理结果
        while (resultSet.next()){
            System.out.println(resultSet.getObject("id"));
            System.out.println(resultSet.getObject("name"));
            System.out.println(resultSet.getObject("chinese"));
            System.out.println(resultSet.getObject("english"));
            System.out.println(resultSet.getObject("math"));
            System.out.println("-----------------");
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

    }
}
